const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  chainWebpack: config => {
    config.watchOptions({
      ignored: /node_modules/
    })
  }
})
